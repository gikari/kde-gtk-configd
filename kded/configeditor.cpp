/*
 * Copyright (C) 2019 Mikhail Zolotukhin <zomial@protonmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDebug>
#include <QDir>
#include <QRegularExpression>
#include <QStandardPaths>
#include <QProcess>

#include <KSharedConfig>
#include <KConfigGroup>

#include <string>
#include <csignal>
#include <cstdio>

#undef signals
#include <gio/gio.h>
#include <gtk/gtk.h>
#define signals Q_SIGNALS

#include "configeditor.h"

void ConfigEditor::setGtk3ConfigValueWayland(const QString &paramName, const QString &paramValue)
{
    gtk_init(nullptr, nullptr);
    g_autoptr(GSettings) gsettings = g_settings_new("org.gnome.desktop.interface");
    g_settings_set_string(gsettings, paramName.toUtf8().constData(), paramValue.toUtf8().constData());
}

void ConfigEditor::setGtk3ConfigValueX11SettingsIni(const QString &paramName, const QString &paramValue)
{
    using qsp = QStandardPaths;
    QString configLocation = qsp::writableLocation(qsp::GenericConfigLocation);
    QString gtk3ConfigPath{configLocation + "/gtk-3.0/settings.ini"};

    KSharedConfig::Ptr gtk3Config = KSharedConfig::openConfig(gtk3ConfigPath, KConfig::NoGlobals);
    KConfigGroup group{gtk3Config, "Settings"};

    group.writeEntry(paramName, paramValue);
    group.sync();
}

void ConfigEditor::setGtk3ConfigValueX11XSettingsd(const QString &paramName, const QString &paramValue)
{
    QString xSettingsdConfigPath {QDir::homePath() + "/.config/xsettingsd/xsettingsd.conf"};
    QFile xSettingsdConfig {xSettingsdConfigPath};
    QString xSettingsdConfigContents {readFileContents(xSettingsdConfig)};
    replaceValueInXSettingsdContents(xSettingsdConfigContents, paramName, paramValue);
    xSettingsdConfig.remove();
    xSettingsdConfig.open(QIODevice::WriteOnly | QIODevice::Text);
    xSettingsdConfig.write(xSettingsdConfigContents.toUtf8());
    reloadXSettingsd();
}

void ConfigEditor::setGtk2ConfigValue(const QString &paramName, const QString &paramValue)
{
    QString gtkrcPath {QDir::homePath() + "/.gtkrc-2.0"};
    QFile gtkrc {gtkrcPath};
    QString gtkrcContents {readFileContents(gtkrc)};
    replaceValueInGtkrcContents(gtkrcContents, paramName, paramValue);
    gtkrc.remove();
    gtkrc.open(QIODevice::WriteOnly | QIODevice::Text);
    gtkrc.write(gtkrcContents.toUtf8());
    reloadGtk2Apps();
}

QString ConfigEditor::readFileContents(QFile &file)
{
    if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return file.readAll();
    } else {
        qWarning() << "Unable to read the " << file.fileName() << " file";
        return "";
    }
}


void ConfigEditor::replaceValueInGtkrcContents(QString &gtkrcContents, const QString &paramName, const QString &paramValue)
{
    QRegularExpression regExp {paramName + "=[^\n]*($|\n)"};

    static const QStringList nonStringProperties{
        "gtk-toolbar-style",
        "gtk-menu-images",
        "gtk-button-images",
        "gtk-primary-button-warps-slider",
    };

    QString newConfigString;
    if (nonStringProperties.contains(paramName)) {
        newConfigString = paramName + "=" + paramValue + "\n";
    } else {
        newConfigString = paramName + "=\"" + paramValue + "\"\n";
    }

    if (gtkrcContents.contains(regExp)) {
        gtkrcContents.replace(regExp, newConfigString);
    } else {
        gtkrcContents = newConfigString + "\n" + gtkrcContents;
    }
}

void ConfigEditor::replaceValueInXSettingsdContents(QString &xSettingsdContents, const QString &paramName, const QString &paramValue)
{
    QRegularExpression regExp {paramName + " [^\n]*($|\n)"};
    QString newConfigString {paramName + " \"" + paramValue + "\"\n"};

    if (xSettingsdContents.contains(regExp)) {
        xSettingsdContents.replace(regExp, newConfigString);
    } else {
        xSettingsdContents = newConfigString + "\n" + xSettingsdContents;
    }
}

void ConfigEditor::reloadGtk2Apps()
{
    QProcess::startDetached(QStandardPaths::findExecutable("gtk2apps_reloader"));
}

void ConfigEditor::reloadXSettingsd()
{
    pid_t pidOfXSettingsd = getPidOfXSettingsd();
    qDebug() << "Pid of XSettings: " << pidOfXSettingsd;
    if (pidOfXSettingsd == 0) {
        qDebug() << "XSettingsd does not exist. Starting new instance...";
        QProcess::startDetached(QStandardPaths::findExecutable("xsettingsd"));
    } else {
        qDebug() << "XSettingsd exists. Sending signal...";
        kill(pidOfXSettingsd, SIGHUP);
    }
}

pid_t ConfigEditor::getPidOfXSettingsd()
{
    char line[512];
    FILE *cmd = popen("pidof -s xsettingsd", "r");
    fgets(line, 512, cmd);
    pclose(cmd);
    return std::atoi(line);
}
