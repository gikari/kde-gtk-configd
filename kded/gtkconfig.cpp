/*
 * Copyright (C) 2019 Mikhail Zolotukhin <zomial@protonmail.com>
 * Copyright (C) 2019 Nicolas Fella <nicolas.fella@gmx.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "gtkconfig.h"

#include <QDebug>
#include <QFont>
#include <QtDBus/QDBusConnection>

#include <KPluginFactory>

K_PLUGIN_CLASS_WITH_JSON(GtkConfig, "gtkconfig.json")

GtkConfig::GtkConfig(QObject *parent, const QVariantList&) : KDEDModule(parent), configEditor {new ConfigEditor()}
{
    qDebug() << "GTK configuration module loaded";
}

void GtkConfig::setFont(const QString &qfontStringRepresentation)
{
    QString configFontName {getConfigFontName(qfontStringRepresentation)};
    configEditor->setGtk2ConfigValue("gtk-font-name", configFontName);
    configEditor->setGtk3ConfigValueWayland("font-name", configFontName);
    configEditor->setGtk3ConfigValueX11SettingsIni("gtk-font-name", configFontName);
    configEditor->setGtk3ConfigValueX11XSettingsd("Gtk/FontName",  configFontName);
}

QString GtkConfig::getConfigFontName(const QString &qfontStringRepresentation)
{
    QFont qfont {};
    qfont.fromString(qfontStringRepresentation);
    return qfont.family()
            + ' ' + qfont.styleName()
            + ' ' + QString::number(qfont.pointSize());
}

#include "gtkconfig.moc"
