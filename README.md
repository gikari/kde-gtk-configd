# KDE GTK Configuration Module

## What is it

This is a kded module, that applies requested settings to GTK applications from KDE Plasma's settings.

## How to use it

To use this module from Qt application you need to create `QDBusInterface` and call some method from it. For example:

```
QFont qfont {};
// ... Set desired qfont properties ...

QDBusInterface kdedInterface {
    QStringLiteral("org.kde.kded5"),
    QStringLiteral("/modules/gtkconfig"),
    QStringLiteral("org.kde.gtkconfig")
};
kdedInterface.call("setFont", qfont.toString());
```
After the calling the module does it's job.

## What it can do

You can call these methods via QDBus Interface:

| Method | Parameters description |
|---|---|
| `setFont(const QString &qFontStringRepresentation)` |  `qFontStringRepresentation` — QFont string representation, which could be obtained via `QFont::toString()` [method.](https://doc.qt.io/qt-5/qfont.html#toString) |


## How it works

GTK Configuration Module modifies various configuration files and then reloads (if necessary) all GTK applications, that currently run on Plasma desktop.

The module writes these configs:

- `.gtkrc-2.0` file in the home directory
- `settings.ini` file in the `$XDG_CONFIG_HOME/gtk-3.0/` directory
- `xsettingsd.conf` file in the `/.config/xsettingsd/` directory
- GNOME GSettings `org.gnome.desktop.interface`

The first config is used to config GTK2 applications. The second and the third for GTK3 applications on X11. The last is used for GTK3 on Wayland session.

Reloading of GTK2 applications is made by small program `gtk2apps_reloader`, which sends signal to all GTK2 apps, making them to reread `.gtkrc-2.0`. Reloading of GTK3 applications is happened automatically on Wayland, however on X11 extra steps are needed. For that purpose XSettingsd daemon is used. The module modifies XSettingsd configuration file and then sends a signal to daemon, which makes it to reload it's configuration.
